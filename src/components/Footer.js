import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { checkDeviceSize } from '../actions/index'

class Footer extends React.Component {
    render () {
        return(
            <div>
            {!this.props.isMobile?
                <div class="footer">
                    <div class="top">
                        <div class="container">
                            <div class="col-lg-4">
                                
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">

                            </div>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="container">
                            <div class="col-lg-4">
                                Sayt <a href="https://www.elitsolutions.com" target="_blank" title="ElitSolutions.com - Web & Mobile Development">ElitSolutions</a> tərəfindən hazırlanıb.
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                &copy; 2017 &nbsp; 
                                <Link to='/' title='Criminal.az - Kriminal Azərbaycan'>
                                    Criminal.az - Kriminal Azərbaycan
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
                :
                ''}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isMobile: state.deviceIsMobile,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Footer)