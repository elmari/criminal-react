import React from 'react'
import { withRouter, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { getNewsByCatId, getMoreCategoryNewsByCount } from '../actions/index'
import Months from '../utils/months'
import NewsItem from './NewsItem'
import {Helmet} from 'react-helmet'
import LazyLoad from 'react-lazyload'

class Category extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            catId: props.match.params.catId,
            newsData: [],
            arraySize: 3,
            isRequested: false,
        }
    }

    componentWillMount = () => {
        window.scrollTo(0, 0);
        this.props.getNewsByCatId(this.state.catId)

        // check if script is loaded. if not, load
        let url = "//code.adsgarden.com/js/adsgarden.js"
        if(document.querySelectorAll(`script[src="${url}"]`).length == 0)
        {
            let rightBanner1st = document.createElement("script")
            rightBanner1st.type = "text/javascript"
            rightBanner1st.charset = "utf-8"
            rightBanner1st.src = url
            document.body.appendChild(rightBanner1st)
        }
    }

    componentDidMount = () => {
        window.addEventListener('scroll', this.handleScroll)
    }

    componentWillUnmount = () => {
        window.removeEventListener('scroll', this.handleScroll)

        this.setState({
            newsData: [],
        })
    }

    componentWillReceiveProps = (nextProps, nextState) => {
        if(this.state.catId != nextProps.match.params.catId)
        {
            this.setState({
                isRequested: true,
                catId: nextProps.match.params.catId
            })
        }

        if(this.state.isRequested)
        {
            this.props.getNewsByCatId(this.state.catId)

            this.setState({
                isRequested: false,
            })
        }
    }

    handleScroll = () => {
        let newArraySize = this.props.newsData.length + 3
        if (
            (window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 100) && 
            this.state.arraySize != newArraySize 
        )
        {
            this.setState({
                arraySize: newArraySize,
            })

            this.props.getMoreCategoryNewsByCount(this.state.catId, newArraySize)
        }
    }

    render () {
        return(
            <div>
            {this.props.isMobile?
                <div className='row'>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>{this.props.newsData.length > 0? this.props.newsData[0].cat_title:''} | Criminal.az - Kriminal Azərbaycan</title>
                </Helmet>
                <ins class="adsgarden" style="display:block; width: 320px; height: 100px;" data-ad-client="273" data-ad-slot="1412"></ins>
                {this.props.newsData.map((item) => 
                    <NewsItem key={item.id} data={item} />
                )}
                </div>
            :
            <div class="container">
                <div class="row manshet">
                    {this.props.manshetData.map(data => 
                        <Link to={'/'+data.link} title={data.title} key={data.id}>
                            <LazyLoad height={140}>
                                <img src={'https://cdn.criminalaz.com'+data.news_img} alt={data.title} title={data.title} class="img-responsive" />
                            </LazyLoad>
                            <p>
                            {data.title}
                            </p>
                        </Link>
                    )}
                </div>
                <div class="row">
                    <div class="col-lg-6" key="1">
                        <ins class="adsgarden" style="display:block; width: 468px; height: 60px;" data-ad-client="273" data-ad-slot="1666"></ins>
                    {this.props.newsData.map((item) => 
                        <NewsItem key={item.id} data={item} />
                    )}
                    </div>
                    <div class="col-lg-3 lastNews">
                    {this.props.last100.map((item) => 
                        <NewsItem key={item.id} data={item} type={'lastNews'} />
                    )}
                    </div>
                    <div class="col-lg-3">
                        <ins class="adsgarden" style="display:block; width: 160px; height: 600px;" data-ad-client="273" data-ad-slot="1515"></ins>
                    </div>
                </div>
            </div>
            }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isMobile: state.deviceIsMobile,
        last20: state.last20,
        last100: state.rightSideForDesktopData.slice(0, 21),
        newsData: state.categoryNewsData,
        manshetData: state.manshetData,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getNewsByCatId: (id) => dispatch(getNewsByCatId(id)),
        getMoreCategoryNewsByCount: (id, numberOfNewsItems) => dispatch(getMoreCategoryNewsByCount(id, numberOfNewsItems)),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Category))