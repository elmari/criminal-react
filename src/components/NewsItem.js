import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Months from '../utils/months'
import LazyLoad from 'react-lazyload';


class NewsItem extends React.Component {
    render () {
        return(
            <div className={'col-xs-12 news_item'}>
                <Link to={'/'+this.props.data.link} title={this.props.data.title}>
                    <div className={'row'}>
                        <div className={'col-xs-2 news_date'}>
                            {this.props.data.day}<br />
                            {Months.getLocalMonthName(this.props.data.month_order)}<br />
                            {this.props.data.news_time}
                        </div>
                        <div className={'col-xs-10 news_title'}>
                            {this.props.data.title}
                        </div>
                    </div>
                    {this.props.type != 'lastNews'?
                        <LazyLoad height={this.props.isMobile?200:360}>
                            <img src={'https://cdn.criminalaz.com'+this.props.data.news_img} className={'img-responsive'} title={this.props.data.title} alt={this.props.data.title} />
                        </LazyLoad>
                    :
                        ''
                    }
                </Link>
                {this.props.isMobile?
                    <Link to={'/'+this.props.data.cat_link} title={this.props.data.cat_title} className={'cat'}>
                    {this.props.data.cat_title}
                    </Link>
                :
                    ''
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isMobile: state.deviceIsMobile,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsItem)