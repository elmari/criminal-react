import React from 'react'
import { withRouter, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { dataIsLoading, getLastNewsByCount, getCategoryLastNewsForDesktopHome } from '../actions/index'
import NewsItem from './NewsItem'
import CategoryNewsBlock from './CategoryNewsBlock'
import {Helmet} from 'react-helmet'
import LazyLoad from 'react-lazyload'

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newsData: [],
            arraySize: 4,
        }
    }

    componentWillMount = () => {
        window.scrollTo(0, 0)

        // check if script is loaded. if not, load
        let url = "//code.adsgarden.com/js/adsgarden.js"
        if(document.querySelectorAll(`script[src="${url}"]`).length == 0)
        {
            let rightBanner1st = document.createElement("script");
            rightBanner1st.type = "text/javascript";
            rightBanner1st.charset = "utf-8";
            rightBanner1st.src = url;
            document.body.appendChild(rightBanner1st);
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll)
        setTimeout(() => this.props.getCategoryLastNewsForDesktopHome(), 1000)
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll)

        this.setState({
            newsData: [],
        })
    }

    componentWillReceiveProps = (nextProps, nextState) => {

    }

    handleScroll = () => {
        let newArraySize = this.props.last20.length + 4
        if (
            (window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 100) && 
            this.state.arraySize != newArraySize 
        )
        {
            this.setState({
                arraySize: newArraySize,
            })

            this.props.getLastNewsByCount(newArraySize)
        }
    }

    render () {
        return(
            <div>
            {this.props.isMobile?
                <div className='row'>
                    <Helmet>
                        <meta charSet="utf-8" />
                        <title>Criminal.az - Kriminal Azərbaycan</title>
                    </Helmet>
                {this.props.last20.map((item, id) => 
                    <NewsItem key={item.id} data={item} />
                )}
                </div>
            :
            <div class="container">
                <div class="row manshet">
                    {this.props.manshetData.map(data => 
                        <Link to={'/'+data.link} title={data.title} key={data.id}>
                            <LazyLoad height={140}>
                                <img src={'https://cdn.criminalaz.com'+data.news_img} alt={data.title} title={data.title} class="img-responsive" />
                            </LazyLoad>
                            <p>
                            {data.title}
                            </p>
                        </Link>
                    )}
                </div>
                <div class="row">
                    <div class="col-lg-6">
                    {this.props.categoryLastNewsForDesktopHomeData.map((item, id) => 
                        <CategoryNewsBlock key={id} categoryData={item} />
                    )}
                    </div>
                    <div class="col-lg-3 lastNews">
                    {this.props.last100.map((item) => 
                        <NewsItem key={item.id} data={item} type={'lastNews'} />
                    )}
                    </div>
                    <div class="col-lg-3 text-centered">
                        <ins class="adsgarden" style="display:block; width: 160px; height: 600px;" data-ad-client="273" data-ad-slot="1515"></ins>
                    </div>
                </div>
            </div>
            }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        last20: state.last20,
        last100: state.rightSideForDesktopData,
        isLoading: state.dataIsLoading,
        isMobile: state.deviceIsMobile,
        manshetData: state.manshetData,
        menuData: state.menuData,
        categoryLastNewsForDesktopHomeData: state.categoryLastNewsForDesktopHomeData,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dataIsLoading: (bool) => dispatch(dataIsLoading(bool)),
        getLastNewsByCount: (numberOfNewsItems) => dispatch(getLastNewsByCount(numberOfNewsItems)),
        getCategoryLastNewsForDesktopHome: () => dispatch(getCategoryLastNewsForDesktopHome()),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home))