/* eslint-disable import/no-named-as-default */
import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router';
import { connect } from 'react-redux'
import { Switch } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import Home from './Home';
import Category from './Category';
import News from './News';
import AboutPage from './AboutPage';
import NotFoundPage from './NotFoundPage';

class App extends React.Component {
  render() {
    return (
      <div className='row'>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/:cat-:catId/:slug-:newsId" component={News} />
          <Route path="/:cat-:catId/" component={Category} />
          <Route path="/about" component={AboutPage} />
          <Route component={NotFoundPage} />
        </Switch>
        <Footer />
      </div>
    );
  }
}

export default App;