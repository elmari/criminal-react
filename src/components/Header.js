import React from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { checkDeviceSize } from '../actions/index';
import Months from '../utils/months'

// import Slider from './Slider'

class Header extends React.Component {
    componentWillMount = () => {
        this.props.checkDeviceSize()
    }

    render () {
        let dateNow = new Date()
        return(
                <div>
                {this.props.isMobile?
                    <div className='row'>
                        <div className={'row'}>
                            <div className={'col-xs-5 logo'}>
                                <Link to='/' title='Criminal.az - Kriminal Azərbaycan'>
                                    <img src={'https://cdn.criminalaz.com/img/logocriminal.png'} className={'logo img-responsive'} title='Criminal.az - Kriminal Azərbaycan' alt='Criminal.az - Kriminal Azərbaycan' />
                                </Link>
                            </div>
                            <div className={'col-xs-7 mezenne'}>
                                <div>
                                    {dateNow.getDate()+' '+Months.getLocalMonthName(dateNow.getMonth()+1)}
                                </div>
                                <div>
                                    Bakı - {this.props.weatherData.temp}&#8451; <img alt='Bakıda hava' title='Bakıda hava' className='weather' src={this.props.weatherData.length !== 0?'https://openweathermap.org/img/w/'+this.props.weatherData.icon+'.png':''} />
                                </div>
                            </div>
                        </div>
                    </div>
                    :
                    <header>
                        <div class="top-bar">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-3 col-sm-4">
                                    {dateNow.getDate()+' '+Months.getLocalMonthName(dateNow.getMonth()+1)}, <img className='weather' src={this.props.weatherData.length !== 0?'https://openweathermap.org/img/w/'+this.props.weatherData.icon+'.png':''} />Bakı - {this.props.weatherData.temp}&#8451;
                                    </div>
                                    <div class="col-lg-6 col-sm-5">
                                        <form class="navbar-form navbar-right search">
                                            <input type="text" class="form-control" placeholder="Axtarış" />
                                        </form>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 pull-right">
                                        <a href="https://twitter.com/Criminal_az?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @Criminal_az</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container top-banners">
                            <div class="row">
                                <div class="col-lg-9 col-lg-3-offset">
                                    <ins class="adsgarden" style="display:block; width: 728px; height: 90px;" data-ad-client="273" data-ad-slot="1665"></ins>
                                </div>
                            </div>
                        </div>
                        <div class="menu">
                            <div class="container">
                                <div class="row">
                                    <div className={'col-lg-2 logo'}>
                                        <Link to='/' title='Criminal.az - Kriminal Azərbaycan'>
                                            <img src={'https://cdn.criminalaz.com/img/logocriminal.png'} className={'logo img-responsive'} title='Criminal.az - Kriminal Azərbaycan' alt='Criminal.az - Kriminal Azərbaycan' />
                                        </Link>
                                    </div>
                                    <div className={'col-lg-10 menu_links'}>
                                    {this.props.menuData.map(data => 
                                        <Link to={'/'+data.link} title={data.title}>
                                        {data.title}
                                        </Link>
                                    )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                }
                </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isMobile: state.deviceIsMobile,
        weatherData: state.weatherData,
        menuData: state.menuData,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        checkDeviceSize: () => dispatch(checkDeviceSize()),
        getInitialData: () => dispatch(getInitialData()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header)