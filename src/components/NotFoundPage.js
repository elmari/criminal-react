import React from 'react';

const NotFoundPage = () => {
  return (
    <div>
      <h4>
        Xəta
      </h4>
      Axtardığınız sorğu üzrə nəticə tapılmadı
    </div>
  );
};

export default NotFoundPage;
