import React from 'react'
import { Link } from 'react-router-dom'
import Months from '../utils/months'
import LazyLoad from 'react-lazyload';

class CategoryNewsBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newsData: [],
        }
    }

    componentWillMount = () => {

    }
    
    render () {
        return(
            <div className="row categoryBlock">
                {this.props.categoryData.length > 0?
                    <div className={'col-lg-12 news_item'}>
                        <Link to={'/'+this.props.categoryData[0].link} title={this.props.categoryData[0].title}>
                            <div className={'row'}>
                                <div className={'col-xs-2 news_date'}>
                                    {new Date().getDate() == parseInt(this.props.categoryData[0].day)?
                                        this.props.categoryData[0].news_time
                                        :
                                        this.props.categoryData[0].day+'\n'+Months.getLocalMonthName(this.props.categoryData[0].month_order)
                                    }
                                </div>
                                <div className={'col-xs-10 news_title'}>
                                    {this.props.categoryData[0].title}
                                </div>
                            </div>
                            <LazyLoad height={310}>
                                <img src={'https://cdn.criminalaz.com'+this.props.categoryData[0].news_img} className={'img-responsive'} title={this.props.categoryData[0].title} alt={this.props.categoryData[0].title} />
                            </LazyLoad>
                            <div className='news_excerpt' dangerouslySetInnerHTML={{__html: this.props.categoryData[0].content.replace(/<\/?[^>]+(>|$)/g, "").substr(0, 240)}} />
                        </Link>
                        <Link to={'/'+this.props.categoryData[0].cat_link} title={this.props.categoryData[0].cat_title} className={'cat'}>
                        {this.props.categoryData[0].cat_title}
                        </Link>
                    </div>
                    :
                    ''
                }

                {this.props.categoryData.length > 1?
                    <div className={'col-lg-6 news_item'}>
                        <Link to={'/'+this.props.categoryData[1].link} title={this.props.categoryData[1].title}>
                            <LazyLoad height={182}>
                                <img src={'https://cdn.criminalaz.com'+this.props.categoryData[1].news_img} className={'img-responsive'} title={this.props.categoryData[1].title} alt={this.props.categoryData[1].title} />
                            </LazyLoad>
                            <div className="news_excerpt">
                            {this.props.categoryData[1].title}
                            </div>
                        </Link>
                    </div>
                    :
                    ''
                }

                {this.props.categoryData.length > 2?
                    <div className={'col-lg-6 news_item'}>
                        <Link to={'/'+this.props.categoryData[2].link} title={this.props.categoryData[2].title}>
                            <img src={'https://cdn.criminalaz.com'+this.props.categoryData[2].news_img} className={'img-responsive'} title={this.props.categoryData[2].title} alt={this.props.categoryData[2].title} />
                            <div className="news_excerpt">
                            {this.props.categoryData[2].title}
                            </div>
                        </Link>
                    </div>
                    :
                    ''
                }
            </div>
        )
    }
}

export default CategoryNewsBlock