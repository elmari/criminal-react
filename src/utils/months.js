export default class Months {
    static getLocalMonthName(month) {
        month = parseInt(month);
        let monthNames = ['Yanvar', 'Fevral', 'Mart', 'Aprel', 'May', 'İyun', 'İyul', 'Avqust', 'Sentyabr', 'Oktyabr', 'Noyabr', 'Dekabr'];
        if(month> 0 && month <13)
            return monthNames[month-1];
    }
}