import Dexie from 'dexie';

const db = new Dexie('CriminalAz');
db.version(1).stores({
    categories: `id, title, link`
});

export default db;