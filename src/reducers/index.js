import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

export function deviceIsMobile(state = false, action) {
  switch (action.type) {
      case 'DEVICE_IS_MOBILE':
          return action.isMobile;

      default:
          return state;
  }
}

export function dataIsLoading(state = false, action) {
  switch (action.type) {
      case 'DATA_IS_LOADING':
          return action.isLoading;

      default:
          return state;
  }
}

export function fetchDataHasErrored(state = false, action) {
  switch (action.type) {
      case 'DATA_HAS_ERRORED':
          return action.hasErrored;

      default:
          return state;
  }
}

export function last20(state = [], action) {
  switch (action.type) {
      case 'INITIAL_DATA_FETCH_SUCCESS':
          return action.jsonData;

      default:
          return state;
  }
}

export function newsData(state = [], action) {
  switch (action.type) {
    case 'DATA_BY_ID_FETCH_SUCCESS':
        return action.jsonData;

    default:
        return state;
  }
}

export function categoryNewsData(state = [], action) {
  switch (action.type) {
    case 'DATA_BY_CAT_ID_FETCH_SUCCESS':
        return action.jsonData;

    default:
        return state;
  }
}

export function similarNewsData(state = [], action) {
    switch (action.type) {
      case 'SIMILAR_DATA_BY_ID_FETCH_SUCCESS':
          return action.jsonData;
  
      default:
          return state;
    }
}

export function weatherData(state = [], action) {
    switch (action.type) {
      case 'WEATHER_DATA_FETCH_SUCCESS':
          return action.jsonData;
  
      default:
          return state;
    }
}

export function menuData(state = [], action) {
    switch (action.type) {
      case 'MENU_DATA_FETCH_SUCCESS':
          return action.jsonData;
  
      default:
          return state;
    }
}

export function manshetData(state = [], action) {
    switch (action.type) {
      case 'MANSHET_DATA_FETCH_SUCCESS':
          return action.jsonData;
  
      default:
          return state;
    }
}

export function categoryLastNewsForDesktopHomeData(state = [], action) {
    switch (action.type) {
      case 'CATEGORY_LAST_NEWS_FOR_DESKTOP_DATA_FETCH_SUCCESS':
          return action.jsonData;
  
      default:
          return state;
    }
}

export function rightSideForDesktopData(state = [], action) {
    switch (action.type) {
      case 'RIGHT_SIDE_DATA_FOR_DESKTOP_FETCH_SUCCESS':
          return action.jsonData
  
      default:
          return state
    }
}

export function photosessionData(state = [], action) {
    switch (action.type) {
        case 'PHOTOSESSION_DATA_BY_ID_FETCH_SUCCESS':
            return action.jsonData

        default: 
            return state
    }
}

const rootReducer = combineReducers({
  deviceIsMobile,
  fetchDataHasErrored,
  last20,
  newsData,
  categoryNewsData,
  dataIsLoading,
  similarNewsData,
  weatherData,
  menuData,
  manshetData,
  categoryLastNewsForDesktopHomeData,
  rightSideForDesktopData,
  photosessionData,
  routing: routerReducer
});

export default rootReducer;
