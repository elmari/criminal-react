import db from '../utils/database';

export function fetchDataHasErrored(bool) {
    return {
        type: 'DATA_HAS_ERRORED',
        hasErrored: bool
    };
}

export const deviceIsMobile = (bool) => {
    return {
        type: 'DEVICE_IS_MOBILE',
        isMobile: bool
    };
}

export const deviceIsTablet = (bool) => {
    return {
        type: 'DEVICE_IS_TABLET',
        isTablet: bool
    };
}

export const deviceIsDesktop = (bool) => {
    return {
        type: 'DEVICE_IS_DESKTOP',
        isDesktop: bool
    };
}

export const deviceIsLargeDesktop = (bool) => {
    return {
        type: 'DEVICE_IS_LARGE_DESKTOP',
        isLargeDesktop: bool
    };
}

export const dataIsLoading = (bool) => {
    return {
        type: 'DATA_IS_LOADING',
        isLoading: bool
    };
}

export function initialDataFetchSuccess(jsonData) {
    return {
        type: 'INITIAL_DATA_FETCH_SUCCESS',
        jsonData
    };
}

export function dataByIdFetchSuccess(jsonData) {
    return {
        type: 'DATA_BY_ID_FETCH_SUCCESS',
        jsonData
    }
}

export function dataByCatIdFetchSuccess(jsonData) {
    return {
        type: 'DATA_BY_CAT_ID_FETCH_SUCCESS',
        jsonData
    }
}

export function similarDataByIdFetchSuccess(jsonData) {
    return {
        type: 'SIMILAR_DATA_BY_ID_FETCH_SUCCESS',
        jsonData
    }
}

export function weatherDataFetchSuccess(jsonData) {
    return {
        type: 'WEATHER_DATA_FETCH_SUCCESS',
        jsonData
    };
}

export function menuDataFetchSuccess(jsonData) {
    return {
        type: 'MENU_DATA_FETCH_SUCCESS',
        jsonData
    };
}

export function manshetDataFetchSuccess(jsonData) {
    return {
        type: 'MANSHET_DATA_FETCH_SUCCESS',
        jsonData
    };
}

export function categoryLastNewsForDesktopHomeSuccess(jsonData) {
    return {
        type: 'CATEGORY_LAST_NEWS_FOR_DESKTOP_DATA_FETCH_SUCCESS',
        jsonData
    };
}

export function rightSideDataForDesktopFetchSuccess(jsonData) {
    return {
        type: 'RIGHT_SIDE_DATA_FOR_DESKTOP_FETCH_SUCCESS',
        jsonData
    };
}

export function photosessionDataByIdFetchSuccess(jsonData) {
    return {
        type: 'PHOTOSESSION_DATA_BY_ID_FETCH_SUCCESS',
        jsonData
    }
}

export const checkDeviceSize = () => {
    let isMobile = window.innerWidth<768?true:false
    let isTablet = window.innerWidth>768 && window.innerWidth < 992?true:false
    let isDesktop = window.innerWidth>992 && window.innerWidth < 1200?true:false
    let isLargeDesktop = window.innerWidth>1200?true:false

    return (dispatch) => {
        if(isMobile)
        {
            dispatch(deviceIsMobile(isMobile))
        }
        else if(isTablet)
        {
            dispatch(deviceIsTablet(isTablet))
        }
        else if(isDesktop)
        {
            // get menus for large desktops
            dispatch(getMenus())
            
            // get manshet for large desktops
            dispatch(getManshet())

            // desktop
            dispatch(deviceIsDesktop(isDesktop))

            // get right side last100 data
            dispatch(getRightSideDataForDesktop())
        }
        else
        {
            // get menus for large desktops
            dispatch(getMenus())

            // get manshet for large desktops
            dispatch(getManshet())

            // large desktop
            dispatch(deviceIsLargeDesktop(isLargeDesktop))

            // get right side last100 data
            dispatch(getRightSideDataForDesktop())
        }

        // get menus for desktops
        dispatch(getMenus())

        dispatch(getWeatherData())

        dispatch(getInitialData())
    }
}

export const getManshet = () => {
    return (dispatch) => {
        // check if we have manshet. get manshet data from localStorage
        let manshet = JSON.parse(localStorage.getItem('manshet'))

        // if we have manshet
        if(manshet !== null)
        {
            // send manshet from localStorage
            dispatch(manshetDataFetchSuccess(manshet))
        }

        // get new manshet from the server
        fetch('https://api.criminalaz.com/news/manshet')
        .then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            }

            return response;
        })
        .then((response) => response.json())
        .then((jsonData) => {
            // set manshet  to localStorage
            localStorage.setItem('manshet', JSON.stringify(jsonData))

            dispatch(manshetDataFetchSuccess(jsonData))
        })
        .catch(() => {
            dispatch(fetchDataHasErrored(true))
        });
    }
}

export const getRightSideDataForDesktop = () => {
    return (dispatch) => {
        // get last20 from localstorage
        let last20 = JSON.parse(localStorage.getItem('last20'))
        
        // check if last20 data length is greater than the given numberOfNewsItems
        if(last20 !== null)
        {
            // get a new array of the given size
            let last70 = last20.slice(0, last20.length>65?65:last20.length)

            dispatch(rightSideDataForDesktopFetchSuccess(last70))
        }
    }
}

export const getMenus = () => {
    return (dispatch) => {
        // check if we have menus. get menus data from localStorage
        let menus = JSON.parse(localStorage.getItem('menus'))

        // if we have menus
        if(menus !== null)
        {
            // send menus from localStorage
            dispatch(menuDataFetchSuccess(menus))
        }
        else
        {
            // get menus from the server
            fetch('https://api.criminalaz.com/category/')
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
    
                return response;
            })
            .then((response) => response.json())
            .then((jsonData) => {
                // set menus  to localStorage
                localStorage.setItem('menus', JSON.stringify(jsonData))
    
                dispatch(menuDataFetchSuccess(jsonData))
            })
            .catch(() => {
                dispatch(fetchDataHasErrored(true))
            });
        }
    }
}

export const getWeatherData = () => {
    return (dispatch) => {
        fetch('https://api.criminalaz.com/weather')
        .then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            }

            return response;
        })
        .then((response) => response.json())
        .then((jsonData) => {
            // set weather data to localStorage
            localStorage.setItem('weather', JSON.stringify(jsonData))

            dispatch(weatherDataFetchSuccess(jsonData))
        })
        .catch(() => {
            dispatch(fetchDataHasErrored(true))
        });
    }
}

export const getCategoryLastNewsForDesktopHome = () => {
    return (dispatch) => {
        
        // get menus from localstorage
        let menus = JSON.parse(localStorage.getItem('menus'))

        // create a new array to set category last news data
        let newArray = []

        // if we have menus
        if(menus !== null)
        {
            // get category last news by category id
            menus.map((item) => {
                // check if we have enough data in category array
                if(localStorage.getItem('catId:'+item.id) !== null)
                {
                    // get category data
                    let categoryArray = JSON.parse(localStorage.getItem('catId:'+item.id))

                    dispatch(reOrderArrayNewsByNewsDate(categoryArray))

                    // create a new array of 4
                    let newArraySize = categoryArray.slice(0,4)

                    newArray.push(newArraySize)
                }
                else
                {
                    fetch('https://api.criminalaz.com/category/'+item.id)
                    .then((response) => {
                        if (!response.ok) {
                            throw Error(response.statusText)
                        }
            
                        return response
                    })
                    .then((response) => response.json())
                    .then((jsonData) => {
                        // create a new array of 4
                        let newArraySize = jsonData.slice(0,4)
                    
                        newArray.push(newArraySize)
                    })
                    .catch(() => {
                        dispatch(fetchDataHasErrored(true))
                    })
                }
            })
        }

        dispatch(categoryLastNewsForDesktopHomeSuccess(newArray))
    }
}

export const getMoreCategoryNewsByCount = (catId, numberOfNewsItems) => {
    return (dispatch) => {
        // get categoryArray from localstorage
        let categoryArray = JSON.parse(localStorage.getItem('catId:'+catId))

        // sort category array by id descending order
        dispatch(reOrderArrayNewsByNewsDate(categoryArray))
        
        // check if categoryArray data length is greater than the given numberOfNewsItems
        if(categoryArray.length > numberOfNewsItems)
        {
            // get a new array of the given size
            let newsItemsToBeSentToCategory = categoryArray.slice(0, numberOfNewsItems)

            dispatch(dataByCatIdFetchSuccess(newsItemsToBeSentToCategory))
        }
        else
        {
            // get previous 20 news item by category oldest news id
            let oldestId = categoryArray[categoryArray.length -1].id


            fetch('https://api.criminalaz.com/category/'+oldestId)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
    
                return response;
            })
            .then((response) => response.json())
            .then((jsonData) => {
                // set the news ids to local storage
                jsonData.map(data => {
                    // set news id to category array
                    let categoryArray = localStorage.getItem('catId:'+catId)
    
                    // if no categoryArray yet
                    if(categoryArray === null)
                    {
                        // set it first time
                        categoryArray = [data]
                    }
                    else
                    {
                        // we already have categoryArray
                        categoryArray = JSON.parse(categoryArray)

                        // check if we have the data in categoryArray
                        if(categoryArray.indexOf(data) === -1)
                        {
                            // add new data to the end of the array
                            categoryArray.push(data)
                        }
                    }
    
                    // set the latest version of the array to localstorage
                    localStorage.setItem('catId:'+data.cat_id, JSON.stringify(categoryArray))
    
                    // set news id seperately
                    localStorage.setItem('newsId:'+data.id, JSON.stringify(data))
                })
    
                // concatenate new arrived news data with the current last20 data
                let newLast20 = last20.concat(jsonData)

                // sort category array by id descending order
                dispatch(reOrderArrayNewsByNewsDate(newLast20))
    
                // save new last20 to localstorage
                localStorage.setItem('last20', JSON.stringify(newLast20))
    
                // get a new array of the given size
                let newsItemsToBeSentToCategory = categoryArray.slice(0, numberOfNewsItems)
            
                dispatch(dataByCatIdFetchSuccess(newsItemsToBeSentToCategory))
            })
            .catch(() => {
                dispatch(fetchDataHasErrored(true))
            });
        }
    } 
}

export const getLastNewsByCount = (numberOfNewsItems) => {
    return (dispatch) => {
        // get last20 from localstorage
        let last20 = JSON.parse(localStorage.getItem('last20'))
        
        // check if last20 data length is greater than the given numberOfNewsItems
        if(last20.length > numberOfNewsItems)
        {
            // get a new array of the given size
            let newsItemsToBeSentToHome = last20.slice(0, numberOfNewsItems)

            dispatch(initialDataFetchSuccess(newsItemsToBeSentToHome))
        }
        else
        {
            // get previous 20 news item by oldest news id
            let oldestId = last20[last20.length -1].id


            fetch('https://api.criminalaz.com/news/prev-'+oldestId)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
    
                return response;
            })
            .then((response) => response.json())
            .then((jsonData) => {
                // set the news ids to local storage
                jsonData.map(data => {
                    // set news id to category array
                    let categoryArray = localStorage.getItem('catId:'+data.cat_id)
    
                    // if no categoryArray yet
                    if(categoryArray === null)
                    {
                        // set it first time
                        categoryArray = [data]
                    }
                    else
                    {
                        // we already have categoryArray
                        categoryArray = JSON.parse(categoryArray)

                        // check if we have the data in categoryArray
                        if(categoryArray.indexOf(data) === -1)
                        {
                            // add new data to the end of the array
                            categoryArray.push(data)
                        }
                    }
    
                    // set the latest version of the array to localstorage
                    localStorage.setItem('catId:'+data.cat_id, JSON.stringify(categoryArray))
    
                    // set news id seperately
                    localStorage.setItem('newsId:'+data.id, JSON.stringify(data))
                })
    
                // concatenate new arrived news data with the current last20 data
                let newLast20 = last20.concat(jsonData)
    
                // save new last20 to localstorage
                localStorage.setItem('last20', JSON.stringify(newLast20))
    
                // get a new array of the given size
                let newsItemsToBeSentToHome = newLast20.slice(0, numberOfNewsItems)
            
                dispatch(initialDataFetchSuccess(newsItemsToBeSentToHome))
            })
            .catch(() => {
                dispatch(fetchDataHasErrored(true))
            });
        }
    }
}

export const getNewAddedNewsDataById = () => {
    return (dispatch) => {
        // get last20 from localstorage
        let last20 = JSON.parse(localStorage.getItem('last20'))

        // if there is no news, 
        if(last20 !== null)
        {
            // get lastId from last20. we will send the lastId to the server
            let lastId = last20[0].id
        
            fetch('https://api.criminalaz.com/news/last-'+lastId)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
    
                return response;
            })
            .then((response) => response.json())
            .then((jsonData) => {
                // set the news ids to local storage
                jsonData.map(data => {
                    // set news id to category array
                    let categoryArray = localStorage.getItem('catId:'+data.cat_id)
    
                    // if no categoryArray yet
                    if(categoryArray === null)
                    {
                        // set it first time
                        categoryArray = [data]
                    }
                    else
                    {
                        // we already have categoryArray
                        categoryArray = JSON.parse(categoryArray)

                        // this function gets newer news items with the given newsId
                        // so, add new data to the beginning of the array
                        categoryArray.unshift(data)
                    }
    
                    // set the latest version of the array to localstorage
                    localStorage.setItem('catId:'+data.cat_id, JSON.stringify(categoryArray))
    
                    // set news id seperately
                    localStorage.setItem('newsId:'+data.id, JSON.stringify(data))
                })
    
                // concatenate new arrived news data with the current last20 data
                let newLast20 = jsonData.concat(last20)
    
                // save new last20 to localstorage
                localStorage.setItem('last20', JSON.stringify(newLast20))

                // create a new array of 4
                let newArraySize = newLast20.slice(0,4)
    
                dispatch(initialDataFetchSuccess(newArraySize))
            })
            .catch(() => {
                dispatch(fetchDataHasErrored(true))
            });
        }
    }
}

export const reOrderArrayNewsByNewsDate = (givenArray) => {
    return (dispatch) => {
        givenArray.sort((a, b)=> 
            b.id - a.id
        )
    }
}

export const getSimilarNewsById = (id) => {
    return (dispatch) => {
        // check if we have this id on localstorage
        if(localStorage.getItem('newsId:'+id) !== null)
        {
            // get news data from localstorage
            let localData = JSON.parse(localStorage.getItem('newsId:'+id))

            // get news category id
            let cat_id = localData.cat_id

            // check if we have data for the category on localstorage
            if(localStorage.getItem('catId:'+cat_id) !== null)
            {
                // we have data for the category on localstorage
                let categoryArray = JSON.parse(localStorage.getItem('catId:'+cat_id))

                let newArray = categoryArray.filter(newsItem => newsItem.id != id)

                dispatch(similarDataByIdFetchSuccess(newArray))
            }
            else
            {
                // we don't have data for the category on localstorage
                dispatch(getNewsByCatId(cat_id))
            }

        }
    }
}

export const getPhotosessionById = (id) => {
    return (dispatch) => {
        // check if we have photosession data on localstorage
        if(localStorage.getItem('newsPhotosession:'+id) !== null)
        {
            // get photosession data from localstorage
            let photosessionData = JSON.parse(localStorage.getItem('newsPhotosession:'+id))

            if(Array.isArray(photosessionData))
                dispatch(photosessionDataByIdFetchSuccess(photosessionData))
            else
                console.log(photosessionData)
        }
        else
        {
            // we don't have photosessiondata on localstorage. fetch it
            fetch('https://api.criminalaz.com/news/photosession-'+id)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
    
                return response;
            })
            .then((response) => response.json())
            .then((jsonData) => {
                // set the photosession to local storage if there is any data
                localStorage.setItem('newsPhotosession:'+id, JSON.stringify(jsonData))

                if(Array.isArray(jsonData))
                    dispatch(photosessionDataByIdFetchSuccess(jsonData))
                else
                    console.log(jsonData)
            })
            .catch(() => {
                dispatch(fetchDataHasErrored(true))
            });
        }
    }
}

export const getNewsById = (id) => {
    return (dispatch) => {
        // get any news data that is added after our initial data fetch
        dispatch(getNewAddedNewsDataById(id))

        // check if we have news data on localstorage
        if(localStorage.getItem('newsId:'+id) !== null)
        {
            // get news data from localstorage
            let localData = JSON.parse(localStorage.getItem('newsId:'+id))

            // dispatch getSimilarNewsById action
            dispatch(getSimilarNewsById(id))

            // dispatch getPhotosessionById action
            // dispatch(getPhotosessionById(id))

            // dispatch dataByIdFetchSuccess action
            dispatch(dataByIdFetchSuccess(localData))
        }
        else
        {
            fetch('https://api.criminalaz.com/news/'+id)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
    
                return response;
            })
            .then((response) => response.json())
            .then((jsonData) => {
                // set the news id to local storage
                localStorage.setItem('newsId:'+jsonData[0].id, JSON.stringify(jsonData[0]))

                // dispatch getPhotosessionById action
                // dispatch(getPhotosessionById(id))

                dispatch(dataByIdFetchSuccess(jsonData[0]));
            })
            .catch(() => {
                dispatch(fetchDataHasErrored(true))
            });
        }

        // get other news in this category
        dispatch(getSimilarNewsById(id))
    }
}

export const getNewsByCatId = (id) => {
    // console.log(id)
    return (dispatch) => {
        // check if we have enough data in category array
        if(localStorage.getItem('catId:'+id) !== null)
        {
            // get category data
            let categoryArray = JSON.parse(localStorage.getItem('catId:'+id))

            dispatch(reOrderArrayNewsByNewsDate(categoryArray))

            // create a new array of 4
            let newArraySize = categoryArray.slice(0,4)

            dispatch(dataByCatIdFetchSuccess(newArraySize))
        }
        else
        {
            fetch('https://api.criminalaz.com/category/'+id)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
    
                return response
            })
            .then((response) => response.json())
            .then((jsonData) => {
                dispatch(dataByCatIdFetchSuccess(jsonData))
            })
            .catch(() => {
                dispatch(fetchDataHasErrored(true))
            })
        }
    }
}

export const getInitialData = () => {
    return (dispatch) => {
        // check if we have last20 data on localstorage
        if(localStorage.getItem('last20') !== null)
        {
            // get news added since the last newsId we have
            dispatch(getNewAddedNewsDataById())
            
            // get last20 data from localstorage
            let localData = JSON.parse(localStorage.getItem('last20'))

            // create a new array of 4
            let newArraySize = localData.slice(0,4)
        
            // dispatch the last20 data
            dispatch(initialDataFetchSuccess(newArraySize))
        }
        else
        {
            fetch('https://api.criminalaz.com/news/last20')
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
    
                return response;
            })
            .then((response) => response.json())
            .then((jsonData) => {
                // set the news ids to local storage
                jsonData.map(data => {
                    // set news id to category array
                    let categoryArray = localStorage.getItem('catId:'+data.cat_id)
    
                    // if no categoryArray yet
                    if(categoryArray === null)
                    {
                        // set it first time
                        categoryArray = [data]
                    }
                    else
                    {
                        // we already have categoryArray
                        categoryArray = JSON.parse(categoryArray)

                        // add new data to the end of the array
                        categoryArray.push(data)
                    }
    
                    // set the latest version of the array to localstorage
                    localStorage.setItem('catId:'+data.cat_id, JSON.stringify(categoryArray))
    
                    // set news id seperately
                    localStorage.setItem('newsId:'+data.id, JSON.stringify(data))
                })
    
                // set last20 items to the localstorage
                localStorage.setItem('last20', JSON.stringify(jsonData))

                // create a new array of 4
                let newArraySize = jsonData.slice(0,4)
            
                // dispatch the last20 data
                dispatch(initialDataFetchSuccess(newArraySize))
            })
            .catch(() => {
                dispatch(fetchDataHasErrored(true))
            });
        }
    }
}