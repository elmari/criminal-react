# install node latest
FROM node:latest

ENV NPM_CONFIG_LOGLEVEL warn

# install nginx express redis
# RUN apt-get -yqq update
# RUN apt-get -yqq install nodejs npm
# RUN ln -s /usr/bin/nodejs /usr/bin/node

# Create app directory
RUN mkdir -p /home/app
WORKDIR /home/app

# Install app dependencies
COPY package.json /home/app
RUN npm install

# Bundle app source
COPY . /home/app

# Build and optimize react app
RUN npm run build

EXPOSE 8080

# defined in package.json
CMD [ "npm", "run", "watch" ]