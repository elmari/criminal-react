// const fs = require('fs')
// const os = require('os')
const webpack = require('webpack')
const path = require('path')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const nodeEnv = process.env.NODE_ENV || 'development';
const isProd = nodeEnv === 'production';

const extractCSS = new ExtractTextPlugin({ filename: 'style.css', disable: false, allChunks: true });

const plugins = [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity,
      filename: 'vendor.bundle.js'
    }),
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify(nodeEnv) }
    }),
    // new HtmlWebpackPlugin({
    //   template: sourcePath + '/index.ejs',
    //   production: isProd,
    //   inject: true,
    // }),
];

const jsEntry = [
    'index',
    'pages/Home',
];

module.exports = {
    entry: [
        './src/index.js'
    ],
    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
        }]
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        "alias": {
            "react": "preact-compat",
            "react-dom": "preact-compat"
          }
    },
    output: {
        path: __dirname + '/dist/js',
        publicPath: '/',
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: './dist',
        headers: { 'Access-Control-Allow-Origin': '*' },
        historyApiFallback: {
            index: 'index.html'
        }
    },
    plugins: [
        // other plugins
        // new UglifyJSPlugin(),
        // new webpack.optimize.OccurrenceOrderPlugin(),
        // new WebpackShellPlugin({
        //     onBuildStart:['echo "Webpack Start"'], 
        //     onBuildEnd:['deploycriminal']
        // })
      ]
};